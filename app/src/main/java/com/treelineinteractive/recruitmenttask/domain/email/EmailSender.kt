package com.treelineinteractive.recruitmenttask.domain.email

interface EmailSender {

    /**
     * @return true if successful, false otherwise
     */
    fun sendEmail(receiver: String, subject: String, text: String): Boolean
}