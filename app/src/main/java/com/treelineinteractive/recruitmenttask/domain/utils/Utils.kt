package com.treelineinteractive.recruitmenttask.domain.utils

val Any.tag: String get() = this.javaClass.simpleName ifIsEmpty "DefaultTag"

infix fun String.ifIsEmpty(value: String): String = if (isEmpty()) value else this