package com.treelineinteractive.recruitmenttask.domain.coroutines

import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope

class CoroutineScopeProvider: ICoroutineScopeProvider {

    @DelicateCoroutinesApi
    override fun getGlobalScope() = GlobalScope

    override fun getIOScope() = CoroutineScope(Dispatchers.IO)
    override fun getMainScope() = CoroutineScope(Dispatchers.Main)
}