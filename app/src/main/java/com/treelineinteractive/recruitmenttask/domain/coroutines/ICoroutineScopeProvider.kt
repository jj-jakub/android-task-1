package com.treelineinteractive.recruitmenttask.domain.coroutines

import kotlinx.coroutines.CoroutineScope

interface ICoroutineScopeProvider {

    fun getGlobalScope(): CoroutineScope
    fun getIOScope(): CoroutineScope
    fun getMainScope(): CoroutineScope
}