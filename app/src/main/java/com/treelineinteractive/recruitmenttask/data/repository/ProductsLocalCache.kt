package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem

class ProductsLocalCache {
    private val savedProducts = mutableListOf<ProductItem>()

    fun replaceProducts(products: List<ProductItem>) {
        savedProducts.clear()
        savedProducts.addAll(products)
    }

    fun getProducts() = savedProducts.toList()
}