package com.treelineinteractive.recruitmenttask.data.network

object ApiConst {
    const val BASE_URL = "https://us-west2-mobile-hiring.cloudfunctions.net"
    const val BOSS_EMAIL_ADDRESS = "bossman@bosscompany.com"
}