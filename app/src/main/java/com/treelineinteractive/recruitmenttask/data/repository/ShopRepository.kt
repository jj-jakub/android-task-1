package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.service.ServiceFactory
import com.treelineinteractive.recruitmenttask.data.network.service.ShopService

class ShopRepository(
    private val productsLocalCache: ProductsLocalCache,
    private val soldProductsCache: SoldProductsCache,
    serviceFactory: ServiceFactory
) {
    private val shopService = serviceFactory.createService<ShopService>()

    suspend fun getProducts(): List<ProductItem> {
        val products = productsLocalCache.getProducts()
        return if (products.isEmpty()) {
            shopService.getInventory()
                .also { fetchedProducts -> productsLocalCache.replaceProducts(fetchedProducts) }
        } else products
    }

    suspend fun refreshProducts(): List<ProductItem> =
        shopService.getInventory().also { products -> productsLocalCache.replaceProducts(products) }

    fun getSalesInfo() = soldProductsCache.getSoldProducts()

    fun getSaleInfo(productId: String) = soldProductsCache.getSoldProducts()
        .find { productSaleInfo -> productSaleInfo.productId == productId }

    fun onProductSold(productId: String) = soldProductsCache.onProductSold(productId)

    fun onProductReturned(productId: String) = soldProductsCache.onProductReturned(productId)
}