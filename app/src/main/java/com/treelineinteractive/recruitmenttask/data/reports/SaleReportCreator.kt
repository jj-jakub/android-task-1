package com.treelineinteractive.recruitmenttask.data.reports

import com.treelineinteractive.recruitmenttask.data.network.ApiConst
import com.treelineinteractive.recruitmenttask.data.repository.ProductSaleInfo
import com.treelineinteractive.recruitmenttask.domain.email.EmailSender

class SaleReportCreator(private val emailSender: EmailSender) {

    companion object {
        private const val REPORT_MAIL_TITLE = "Daily sales of products"
    }

    fun createAndSendReportToBoss(salesInformation: List<ProductSaleInfo>) {
        val reportContent = createSaleReportContent(salesInformation)
        emailSender.sendEmail(ApiConst.BOSS_EMAIL_ADDRESS, REPORT_MAIL_TITLE, reportContent)
    }

    private fun createSaleReportContent(salesInformation: List<ProductSaleInfo>): String {
        val firstSentence = "Hello Boss, below you can find report from today's sales.\n"
        var productSaleInfoTexts = ""

        salesInformation.forEach { saleInfo ->
            productSaleInfoTexts += createProductSaleInfoText(saleInfo)
        }

        return firstSentence + productSaleInfoTexts
    }

    private fun createProductSaleInfoText(saleInfo: ProductSaleInfo): String {
        return "\nid: ${saleInfo.productId}\n\n" +
                "sold: ${saleInfo.numberOfSold}\n\n"
    }
}