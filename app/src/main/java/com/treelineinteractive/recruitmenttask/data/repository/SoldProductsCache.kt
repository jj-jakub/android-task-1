package com.treelineinteractive.recruitmenttask.data.repository

import java.lang.Integer.max

data class ProductSaleInfo(val productId: String, val numberOfSold: Int)

class SoldProductsCache {
    private val soldProductsInfo = mutableListOf<ProductSaleInfo>()

    @Synchronized
    fun onProductSold(productId: String): ProductSaleInfo {
        val productSaleInfo =
            soldProductsInfo.find { productSaleInfo -> productSaleInfo.productId == productId }

        return if (productSaleInfo == null) {
            val newProductSaleInfo = ProductSaleInfo(productId, 1)
            soldProductsInfo.add(newProductSaleInfo)
            newProductSaleInfo
        } else {
            val updatedSaleInfo = productSaleInfo.copy(numberOfSold = productSaleInfo.numberOfSold + 1)
            soldProductsInfo.remove(productSaleInfo)
            soldProductsInfo.add(updatedSaleInfo)
            updatedSaleInfo
        }
    }

    @Synchronized
    fun onProductReturned(productId: String): ProductSaleInfo {
        val productSaleInfo =
            soldProductsInfo.find { productSaleInfo -> productSaleInfo.productId == productId }

        return if (productSaleInfo == null) {
            val newProductSaleInfo = ProductSaleInfo(productId, 0)
            soldProductsInfo.add(newProductSaleInfo)
            newProductSaleInfo
        } else {
            val numberOfSold = max(0, productSaleInfo.numberOfSold - 1)
            val updatedSaleInfo = productSaleInfo.copy(numberOfSold = numberOfSold)
            soldProductsInfo.remove(productSaleInfo)
            soldProductsInfo.add(updatedSaleInfo)
            updatedSaleInfo
        }
    }


    fun getSoldProducts() = soldProductsInfo.toList()
}