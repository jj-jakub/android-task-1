package com.treelineinteractive.recruitmenttask.app

import android.app.Application
import com.treelineinteractive.recruitmenttask.di.koin.mainModule
import org.koin.core.context.startKoin
import org.koin.android.ext.koin.androidContext

class RecruitmentTaskApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        launchKoin()
    }

    private fun launchKoin() {
        startKoin {
            androidContext(this@RecruitmentTaskApplication)
            modules(mainModule)
        }
    }
}