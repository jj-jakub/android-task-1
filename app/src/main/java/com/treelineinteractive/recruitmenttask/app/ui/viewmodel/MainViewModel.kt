package com.treelineinteractive.recruitmenttask.app.ui.viewmodel

import android.util.Log
import com.treelineinteractive.recruitmenttask.app.ui.viewmodel.MainViewModel.MainViewAction
import com.treelineinteractive.recruitmenttask.app.ui.viewmodel.MainViewModel.MainViewAction.LoadingProducts
import com.treelineinteractive.recruitmenttask.app.ui.viewmodel.MainViewModel.MainViewAction.ProductsLoaded
import com.treelineinteractive.recruitmenttask.app.ui.viewmodel.MainViewModel.MainViewAction.ProductsLoadingError
import com.treelineinteractive.recruitmenttask.app.ui.viewmodel.MainViewModel.MainViewState
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.reports.SaleReportCreator
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import com.treelineinteractive.recruitmenttask.domain.coroutines.ICoroutineScopeProvider
import com.treelineinteractive.recruitmenttask.domain.utils.tag
import kotlinx.coroutines.launch
import org.koin.java.KoinJavaComponent.inject

class MainViewModel :
    BaseViewModel<MainViewState, MainViewAction>(MainViewState()) {

    data class MainViewState(
        val isLoading: Boolean = false,
        val error: String? = null,
        val items: List<ProductItem> = listOf()
    ) : BaseViewState {
        val isSuccess: Boolean
            get() = !isLoading && error == null
    }

    sealed class MainViewAction : BaseAction {
        object LoadingProducts : MainViewAction()
        data class ProductsLoaded(val items: List<ProductItem>) : MainViewAction()
        data class ProductsLoadingError(val error: String) : MainViewAction()
    }

    private val shopRepository: ShopRepository by inject(ShopRepository::class.java)
    private val coroutineScopeProvider: ICoroutineScopeProvider by inject(ICoroutineScopeProvider::class.java)
    private val saleReportCreator: SaleReportCreator by inject(SaleReportCreator::class.java)

    fun loadProducts() {
        fetchProducts(shopRepository::getProducts)
    }

    fun refreshProducts() {
        fetchProducts(shopRepository::refreshProducts)
    }

    fun getProductSaleInfo(productId: String) = shopRepository.getSaleInfo(productId)

    fun onProductSold(productId: String) = shopRepository.onProductSold(productId)

    fun onProductReturned(productId: String) = shopRepository.onProductReturned(productId)

    fun sendSaleReport() {
        saleReportCreator.createAndSendReportToBoss(shopRepository.getSalesInfo())
    }

    override fun onReduceState(viewAction: MainViewAction): MainViewState = when (viewAction) {
        is LoadingProducts -> state.copy(isLoading = true, error = null)
        is ProductsLoaded -> state.copy(
            isLoading = false,
            error = null,
            items = viewAction.items
        )
        is ProductsLoadingError -> state.copy(
            isLoading = false,
            error = viewAction.error
        )
    }

    private fun fetchProducts (productGetterMethod: suspend () -> List<ProductItem>) {
        coroutineScopeProvider.getGlobalScope().launch {
            sendAction(LoadingProducts)
            try {
                sendAction(ProductsLoaded(productGetterMethod.invoke()))
            } catch (e: Exception) {
                Log.e(tag, ITEMS_FETCHING_EXCEPTION_MESSAGE, e)
                val errorMessage = e.message ?: ITEMS_FETCHING_EXCEPTION_MESSAGE
                sendAction(ProductsLoadingError(errorMessage))
            }
        }
    }

    companion object {
        private const val ITEMS_FETCHING_EXCEPTION_MESSAGE =
            "Exception occurred while getting items from server"
    }
}