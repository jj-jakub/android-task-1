package com.treelineinteractive.recruitmenttask.app.ui.activity

import com.treelineinteractive.recruitmenttask.data.repository.ProductSaleInfo

interface ProductViewContainer {
    fun getSaleInfo(productId: String): ProductSaleInfo?
    fun onProductSold(productId: String): ProductSaleInfo
    fun onProductReturned(productId: String): ProductSaleInfo
}