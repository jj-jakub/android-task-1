package com.treelineinteractive.recruitmenttask.app.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout.LayoutParams
import androidx.recyclerview.widget.RecyclerView
import com.treelineinteractive.recruitmenttask.app.ui.activity.ProductViewContainer
import com.treelineinteractive.recruitmenttask.app.ui.adapter.ProductItemViewAdapter.ProductItemViewHolder
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ViewProductItemBinding

class ProductItemViewAdapter(
    private val productViewContainer: ProductViewContainer,
    private val products: List<ProductItem>
) :
    RecyclerView.Adapter<ProductItemViewHolder>() {

    inner class ProductItemViewHolder(private val binding: ViewProductItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int) {
            val productItem = products[position]
            binding.root.layoutParams = createLayoutParams()
            binding.nameLabel.text = productItem.title
            binding.descriptionLabel.text = productItem.description
            binding.availableItemsValue.text = productItem.available.toString()

            val saleData = productViewContainer.getSaleInfo(productItem.id)
            binding.soldItemsValue.text = saleData?.numberOfSold?.toString() ?: "0"

            binding.itemSoldButton.setOnClickListener {
                val updatedInfo = productViewContainer.onProductSold(productItem.id)
                binding.soldItemsValue.text = updatedInfo.numberOfSold.toString()
            }

            binding.itemReturnedButton.setOnClickListener {
                val updatedInfo = productViewContainer.onProductReturned(productItem.id)
                binding.soldItemsValue.text = updatedInfo.numberOfSold.toString()
            }
        }

        private fun createLayoutParams(): LayoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.WRAP_CONTENT
        ).apply {
            topMargin = PRODUCT_VIEW_TOP_MARGIN
            marginEnd = PRODUCT_VIEW_END_MARGIN
            marginStart = PRODUCT_VIEW_START_MARGIN
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductItemViewHolder {
        val binding =
            ViewProductItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ProductItemViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProductItemViewHolder, position: Int) {
        holder.bind(position)
    }

    override fun getItemCount(): Int = products.size

    companion object {
        private const val PRODUCT_VIEW_TOP_MARGIN = 16
        private const val PRODUCT_VIEW_END_MARGIN = 16
        private const val PRODUCT_VIEW_START_MARGIN = 16
    }
}