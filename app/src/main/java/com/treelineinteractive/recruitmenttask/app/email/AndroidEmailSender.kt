package com.treelineinteractive.recruitmenttask.app.email

import android.content.Context
import android.content.Intent
import android.net.Uri
import com.treelineinteractive.recruitmenttask.domain.email.EmailSender

class AndroidEmailSender(private val context: Context) : EmailSender {

    override fun sendEmail(receiver: String, subject: String, text: String): Boolean {
        val intent = Intent(Intent.ACTION_SENDTO).apply {
            data = Uri.parse("mailto:")
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            putExtra(Intent.EXTRA_EMAIL, arrayOf(receiver))
            putExtra(Intent.EXTRA_SUBJECT, subject)
            putExtra(Intent.EXTRA_TEXT, text)
        }

        if (intent.resolveActivity(context.packageManager) != null) {
            context.startActivity(intent)
            return true
        }
        return false
    }
}