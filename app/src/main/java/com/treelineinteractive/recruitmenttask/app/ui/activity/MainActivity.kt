package com.treelineinteractive.recruitmenttask.app.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.databinding.ActivityMainBinding
import com.treelineinteractive.recruitmenttask.app.ui.viewmodel.MainViewModel.MainViewState
import com.treelineinteractive.recruitmenttask.app.ui.adapter.ProductItemViewAdapter
import com.treelineinteractive.recruitmenttask.app.ui.observer
import com.treelineinteractive.recruitmenttask.app.ui.viewBinding
import com.treelineinteractive.recruitmenttask.app.ui.viewmodel.MainViewModel

class MainActivity : AppCompatActivity(), ProductViewContainer {

    private val binding by viewBinding(ActivityMainBinding::inflate)
    private lateinit var mainViewModel: MainViewModel
    private val productItemList = mutableListOf<ProductItem>()

    private val productItemViewAdapter = ProductItemViewAdapter(this, productItemList)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        mainViewModel = ViewModelProvider(this).get(MainViewModel::class.java)
        mainViewModel.loadProducts()

        setupOnClickListeners()

        binding.itemsLayout.apply {
            adapter = productItemViewAdapter
            layoutManager = LinearLayoutManager(this@MainActivity)
        }

        mainViewModel.stateLiveData.observe {
            state -> onStateLiveDataChanged(state)
        }
    }

    private fun setupOnClickListeners() {
        binding.apply {
            retryButton.setOnClickListener {
                mainViewModel.refreshProducts()
            }

            sendEmailButton.setOnClickListener {
                mainViewModel.sendSaleReport()
            }
        }
    }

    override fun getSaleInfo(productId: String) = mainViewModel.getProductSaleInfo(productId)

    override fun onProductSold(productId: String) = mainViewModel.onProductSold(productId)

    override fun onProductReturned(productId: String) = mainViewModel.onProductReturned(productId)

    private fun onStateLiveDataChanged(state: MainViewState) {
        binding.progressBar.isVisible = state.isLoading
        binding.errorLayout.isVisible = state.error != null
        binding.errorLabel.text = state.error

        binding.itemsLayout.isVisible = state.isSuccess
        binding.sendEmailButton.isVisible = state.isSuccess

        if (state.isSuccess) {
            productItemList.clear()
            productItemList.addAll(state.items)
            productItemViewAdapter.notifyDataSetChanged()
        }
    }

    private fun <T> LiveData<T>.observe(onChanged: (T) -> Unit) {
        observer(this@MainActivity, onChanged)
    }
}