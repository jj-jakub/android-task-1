package com.treelineinteractive.recruitmenttask.di.koin

import com.treelineinteractive.recruitmenttask.data.network.service.ServiceFactory
import com.treelineinteractive.recruitmenttask.data.repository.ProductsLocalCache
import com.treelineinteractive.recruitmenttask.data.reports.SaleReportCreator
import com.treelineinteractive.recruitmenttask.data.repository.ShopRepository
import com.treelineinteractive.recruitmenttask.data.repository.SoldProductsCache
import com.treelineinteractive.recruitmenttask.domain.coroutines.CoroutineScopeProvider
import com.treelineinteractive.recruitmenttask.domain.coroutines.ICoroutineScopeProvider
import com.treelineinteractive.recruitmenttask.app.email.AndroidEmailSender
import com.treelineinteractive.recruitmenttask.domain.email.EmailSender
import org.koin.dsl.module

val mainModule = module {
    single { ServiceFactory() }
    single { ProductsLocalCache() }
    single { SoldProductsCache() }
    single { ShopRepository(get(), get(), get()) }
    single <EmailSender> { AndroidEmailSender(get()) }
    single { SaleReportCreator(get()) }

    factory <ICoroutineScopeProvider> { CoroutineScopeProvider() }
}