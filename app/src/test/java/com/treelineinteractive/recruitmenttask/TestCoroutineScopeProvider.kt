package com.treelineinteractive.recruitmenttask

import com.treelineinteractive.recruitmenttask.domain.coroutines.ICoroutineScopeProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope

@ExperimentalCoroutinesApi
class TestCoroutineScopeProvider: ICoroutineScopeProvider {

    private val testCoroutineDispatcher = TestCoroutineDispatcher()

    override fun getGlobalScope(): TestCoroutineScope = TestCoroutineScope(testCoroutineDispatcher)
    override fun getIOScope(): TestCoroutineScope = TestCoroutineScope(testCoroutineDispatcher)
    override fun getMainScope(): TestCoroutineScope = TestCoroutineScope(testCoroutineDispatcher)

}