package com.treelineinteractive.recruitmenttask.data.reports

import com.treelineinteractive.recruitmenttask.data.network.ApiConst
import com.treelineinteractive.recruitmenttask.data.repository.ProductSaleInfo
import com.treelineinteractive.recruitmenttask.domain.email.EmailSender
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SaleReportCreatorTest {

    @RelaxedMockK
    private lateinit var emailSender: EmailSender

    private lateinit var saleReportCreator: SaleReportCreator

    @BeforeEach
    fun setup() {
        MockKAnnotations.init(this)
        saleReportCreator = SaleReportCreator(emailSender)
    }

    @Test
    fun `should try to send email to proper address`() {
        saleReportCreator.createAndSendReportToBoss(listOf())
        val addressSlot = slot<String>()
        verify (exactly = 1) { emailSender.sendEmail(capture(addressSlot), any(), any()) }
        assertEquals(ApiConst.BOSS_EMAIL_ADDRESS, addressSlot.captured)
    }

    @Test
    fun `generated report should contain ids of products that have been sold`() {
        val firstProductId = "1111-1111-1111-1111"
        val secondProductId = "2222-2222-2222-2222"
        val salesInfo = listOf(
            ProductSaleInfo(firstProductId, 5),
            ProductSaleInfo(secondProductId, 10)
        )
        val emailContentSlot = slot<String>()

        saleReportCreator.createAndSendReportToBoss(salesInfo)
        verify (exactly = 1) { emailSender.sendEmail(any(), any(), capture(emailContentSlot)) }
        assertTrue(emailContentSlot.captured.contains(firstProductId))
        assertTrue(emailContentSlot.captured.contains(secondProductId))
    }
}