package com.treelineinteractive.recruitmenttask.data.network.service

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ServiceFactoryTest {

    private lateinit var serviceFactory: ServiceFactory

    @BeforeEach
    fun setup() {
        serviceFactory = ServiceFactory()
    }

    @Test
    fun `should create retrofit with proper baseUrl`() {
        val baseUrl = "http://baseurl/"
        val retrofit = serviceFactory.retrofit(baseUrl)

        assertEquals(baseUrl, retrofit.baseUrl().toString())
    }
}