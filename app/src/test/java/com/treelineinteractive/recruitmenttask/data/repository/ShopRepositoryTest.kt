package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.TestCoroutineScopeProvider
import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import com.treelineinteractive.recruitmenttask.data.network.service.ServiceFactory
import com.treelineinteractive.recruitmenttask.data.network.service.ShopService
import io.mockk.MockKAnnotations
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

@ExperimentalCoroutinesApi
class ShopRepositoryTest {

    @RelaxedMockK
    private lateinit var productsLocalCache: ProductsLocalCache

    @MockK
    private lateinit var soldProductsCache: SoldProductsCache

    @MockK
    private lateinit var serviceFactory: ServiceFactory

    @RelaxedMockK
    private lateinit var shopService: ShopService

    private lateinit var shopRepository: ShopRepository

    private lateinit var testCoroutineScopeProvider: TestCoroutineScopeProvider

    @BeforeEach
    fun setup() {
        MockKAnnotations.init(this)
        testCoroutineScopeProvider = TestCoroutineScopeProvider()

        every { serviceFactory.createService<ShopService>() } returns shopService
        shopRepository = ShopRepository(productsLocalCache, soldProductsCache, serviceFactory)
    }

    @Test
    fun `should ask shopService for products when refreshing`() =
        testCoroutineScopeProvider.getIOScope().runBlockingTest {
            coEvery { shopService.getInventory() } returns listOf()

            shopRepository.refreshProducts()

            coVerify(exactly = 1) { shopService.getInventory() }
        }

    @Test
    fun `should ask shopService for products if cache is empty`() =
        testCoroutineScopeProvider.getIOScope().runBlockingTest {
            every { productsLocalCache.getProducts() } returns listOf()
            coEvery { shopService.getInventory() } returns listOf()

            shopRepository.getProducts()

            coVerify(exactly = 1) { shopService.getInventory() }
        }

    @Test
    fun `should not ask shopService for products if cache is not empty`() =
        testCoroutineScopeProvider.getIOScope().runBlockingTest {
            val mockProduct = mockk<ProductItem>()
            every { productsLocalCache.getProducts() } returns listOf(mockProduct)

            shopRepository.getProducts()

            coVerify(exactly = 0) { shopService.getInventory() }
        }
}