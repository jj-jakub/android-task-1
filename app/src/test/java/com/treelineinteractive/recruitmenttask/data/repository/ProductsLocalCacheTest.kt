package com.treelineinteractive.recruitmenttask.data.repository

import com.treelineinteractive.recruitmenttask.data.network.model.ProductItem
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class ProductsLocalCacheTest {

    private lateinit var productsLocalCache: ProductsLocalCache

    @BeforeEach
    fun setup() {
        productsLocalCache = ProductsLocalCache()
    }

    @Test
    fun `cache should be empty after initialization`() {
        assertTrue(productsLocalCache.getProducts().isEmpty())
    }

    @Test
    fun `should remove existing items when replacing with new ones`() {
        val firstItem = mockk<ProductItem>()
        val secondItem = mockk<ProductItem>()
        val initialList = listOf(firstItem, secondItem)
        productsLocalCache.replaceProducts(initialList)

        val thirdItem = mockk<ProductItem>()
        val fourthItem = mockk<ProductItem>()
        val finalList = listOf(thirdItem, fourthItem)
        productsLocalCache.replaceProducts(finalList)

        assertEquals(finalList, productsLocalCache.getProducts())
    }

}