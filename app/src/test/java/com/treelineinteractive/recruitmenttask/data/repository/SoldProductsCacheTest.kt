package com.treelineinteractive.recruitmenttask.data.repository

import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class SoldProductsCacheTest {

    private lateinit var soldProductsCache: SoldProductsCache

    @BeforeEach
    fun setup() {
        soldProductsCache = SoldProductsCache()
    }

    @Test
    fun `cache should be empty after initialization`() {
        assertTrue(soldProductsCache.getSoldProducts().isEmpty())
    }

    @Test
    fun `empty cache should create product sale info after calling onProductSold`() {
        val productId = "1111"
        soldProductsCache.onProductSold(productId)
        val items = soldProductsCache.getSoldProducts()
        assertEquals(1, items.size)
    }

    @Test
    fun `empty cache should proper create product sale info after calling onProductSold`() {
        val productId = "1111"
        soldProductsCache.onProductSold(productId)
        val items = soldProductsCache.getSoldProducts()
        assertEquals(productId, items.first().productId)
        assertEquals(1, items.first().numberOfSold)
    }

    @Test
    fun `empty cache should create product sale info after calling onProductReturned`() {
        val productId = "1111"
        soldProductsCache.onProductReturned(productId)
        val items = soldProductsCache.getSoldProducts()
        assertEquals(1, items.size)
    }

    @Test
    fun `empty cache should proper create product sale info after calling onProductReturned`() {
        val productId = "1111"
        soldProductsCache.onProductReturned(productId)
        val items = soldProductsCache.getSoldProducts()
        assertEquals(productId, items.first().productId)
        assertEquals(0, items.first().numberOfSold)
    }

    @Test
    fun `cache should properly increment number of sold products`() {
        val productId = "1111"
        val timesSold = 5
        repeat(timesSold) {
            soldProductsCache.onProductSold(productId)
        }

        assertEquals(timesSold, soldProductsCache.getSoldProducts().first().numberOfSold)
    }

    @Test
    fun `cache should properly decrement number of sold products`() {
        val productId = "1111"
        val timesSold = 5
        repeat(timesSold) {
            soldProductsCache.onProductSold(productId)
        }

        val timesReturned = 3
        repeat(timesReturned) {
            soldProductsCache.onProductReturned(productId)
        }

        assertEquals(
            timesSold - timesReturned,
            soldProductsCache.getSoldProducts().first().numberOfSold
        )
    }

    @Test
    fun `cache should not decrement number of sold products to value less than zero`() {
        val productId = "1111"
        val timesSold = 5
        repeat(timesSold) {
            soldProductsCache.onProductSold(productId)
        }

        val timesReturned = 10
        repeat(timesReturned) {
            soldProductsCache.onProductReturned(productId)
        }

        assertEquals(0, soldProductsCache.getSoldProducts().first().numberOfSold)
    }
}